import { GuitarAppPage } from './app.po';

describe('guitar-app App', () => {
  let page: GuitarAppPage;

  beforeEach(() => {
    page = new GuitarAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
